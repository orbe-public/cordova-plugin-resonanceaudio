---
title: Cordova Resonance Audio Plugin
description: Resonance Audio for Cordova, based on the C/C++ Resonance Audio API.
---
<!--
The MIT License (MIT)
Copyright © 2021 Orbe

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-->
# Cordova Resonance Audio Plugin

Resonance Audio is a multi-platform spatial audio SDK, delivering high fidelity at scale. This powerful spatial audio technology is critical to realistic experiences for AR, VR, gaming, and video.

https://resonance-audio.github.io/resonance-audio/discover/overview



## Installation
```
cordova plugin add https://gitlab.com/orbe-public/cordova-plugin-resonanceaudio.git
```

## Supported Platforms
* Android
* IOS



## Features

@todo



## Methods

@todo



## Example

```js
@todo
```



## Requirements

iOS 9.0 and later.  
Android 8.1.0 (API level 27) and later.
