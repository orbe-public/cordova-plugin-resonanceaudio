// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once

#include <memory>
#include "resonance_audio_api.h"
#include "room_properties.h"

#ifdef __ANDROID__
#include <android/log.h>
#endif


using namespace vraudio;
namespace ResonanceAudioPlugin {

    // ---------------------------------------------------------------------------------------------
        #pragma mark log
    // ---------------------------------------------------------------------------------------------

    #ifndef __ANDROID__
    void NSLog_d(const char* message);
    void NSLog_w(const char* message);
    void NSLog_e(const char* message);
    #endif

    /**
     * Log message:
     * use log() for verbose messages
     * use warn() for warning messages
     * use error() for error messages
     */
    void Log_d( const char* format, ... );
    void Log_w( const char* format, ... );
    void Log_e( const char* format, ... );




    // ---------------------------------------------------------------------------------------------
        #pragma mark Resonance Audio
    // ---------------------------------------------------------------------------------------------

    // number of (DSP) output channels.
    static const size_t kOutputChannels = 2;

    /**
     * Stores the necessary components for the ResonanceAudio system.
     *
     * @param sample_rate_hz System sample rate.
     * @param num_channels Number of channels of audio output.
     * @param frames_per_buffer Number of frames per buffer.
     *
    struct System {
        System(int sample_rate_hz, size_t num_channels, size_t frames_per_buffer)
        //: api(CreateResonanceAudioApi(num_channels, frames_per_buffer, sample_rate_hz))
        {
            api = std::unique_ptr<ResonanceAudioApi>(vraudio::CreateResonanceAudioApi(num_channels, frames_per_buffer, sample_rate_hz));
            Log_d("Resonance Audio system created");
        }

        // VrAudio API instance to communicate with the internal system.
        std::unique_ptr<ResonanceAudioApi> api;
        
//        // Current room properties of the environment.
//        RoomProperties room_properties;
    };*/

    /**
     * Represents the current state, including room effects state.
     */
    struct Scene {
        Scene() : sound_speed(1.0f), renderingmode(RenderingMode::kBinauralLowQuality) {}
        
        // gain applied to all sources (stored here in dB).
        //float gain;
        // default room properties, which effectively disable the room effects.
        RoomProperties room;
        
        float sound_speed;
        // source rendering mode / ambisonics order
        RenderingMode renderingmode;

//        // Current room properties of the environment.
//        ReflectionProperties reflection;
//        ReverbProperties reverberation;
    };

    /**
     * Respresent the current listener
     */
    struct Listener {
        // Constructs |RoomProperties| with the default values.
        Listener() : position{0.0f, 0.0f, 0.0f}, forward{0.0f, 0.0f, 0.0f}, up{0.0f, 0.0f, 0.0f} {}
        //rotation{0.0f, 0.0f, 0.0f, 1.0f}, orientation{0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f} {}
        
        float position[3];
        float forward[3];
        float up[3];
//        float orientation[6];
//        float rotation[4];
    };

    /**
     * Respresent an audio source
     */
    struct Source {
        // Constructs  with the default values.
        Source(ResonanceAudioApi::SourceId sid) : id(sid),
            position{0.0f, 0.0f, 0.0f},
            forward{0.0f, 0.0f, -1.0f},
            up{0.0f, 1.0f, 0.0f},
            rolloff_model(DistanceRolloffModel::kLogarithmic),
            min_distance(1.0f),
            max_distance(1000.0f),
            distance(0.0f)
            {}
        
        ResonanceAudioApi::SourceId id;
        float position[3];
        float forward[3];
        float up[3];
        
        // model for the distance rolloff effects
        DistanceRolloffModel rolloff_model;
        // minimum distance for the distance rolloff effects
        float min_distance;
        // maximum distance for the distance rolloff effects
        float max_distance;
        // current distance between source and listener
        float distance;
    };



    //static void *audiocontext = nullptr;
    void initialize(void *context = nullptr);
    void close();

    //System* createSystem(int sample_rate_hz, size_t num_channels, size_t frames_per_buffer);
    void createSystem(int sample_rate_hz, size_t num_channels, size_t frames_per_buffer);

    ResonanceAudioApi::SourceId createSource(size_t num_channels);
    void destroySource(ResonanceAudioApi::SourceId id);

    void enableRoomEffects(bool enable);
    bool setRoomProperties(RoomProperties properties);
    //bool setRoomProperties(float dimensions[], MaterialName materials[]);
    //bool setSpeedOfSound(float speed);
    bool setListenerOrientation(float forwardX, float forwardY, float forwardZ, float upX, float upY, float upZ);
    bool setListenerPosition(float x, float y, float z);
    
    bool setSourceRenderingMode(int mode);
    void setSourcePosition(ResonanceAudioApi::SourceId id, float x, float y, float z);
    void setSourceOrientation(ResonanceAudioApi::SourceId id, float forwardX, float forwardY, float forwardZ, float upX, float upY, float upZ);
    void setSourceDistanceModel(ResonanceAudioApi::SourceId id, DistanceRolloffModel model, float min_distance, float max_distance);
    void setSourceRolloffModel(ResonanceAudioApi::SourceId id, DistanceRolloffModel model);
    void setSourceMinDistance(ResonanceAudioApi::SourceId id, float min_distance);
    void setSourceMaxDistance(ResonanceAudioApi::SourceId id, float max_distance);
    void setSourceRoomEffectsGain(ResonanceAudioApi::SourceId id, float gain);
    void setSourceVolume(ResonanceAudioApi::SourceId id, float volume);
    void setSourceDirectivityPattern(ResonanceAudioApi::SourceId id, float alpha, float sharpness);
    void setSourceSpread(ResonanceAudioApi::SourceId id, float degree);
    void setSourceNearEffectGain(ResonanceAudioApi::SourceId id, float gain);
    void setSourceOcclusionIntensity(ResonanceAudioApi::SourceId id, float intensity);


    // sets the next audio buffer
    void fill(ResonanceAudioApi::SourceId id, const float* input, int num_channels, size_t length);
    // renders and outputs the output buffer
    void render(const float* input, float* output, size_t length);
    //void mix(const float* input, float* output, size_t length);


} // namespace ResonanceAudioPlugin
