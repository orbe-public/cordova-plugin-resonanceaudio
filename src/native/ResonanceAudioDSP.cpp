// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "ResonanceAudioDSP.h"
#include "ResonanceAudioPlugin.h"





namespace ResonanceAudioPlugin {

// -------------------------------------------------------------------------------------------------
// FMOD DSP
// -------------------------------------------------------------------------------------------------

namespace DSP {
#if CORDOVA_PLUGIN_FMOD
namespace fmod {

// -- Listener  ------------------------------------------------------------------------------------

// the main / single DSP
static FMOD::DSP *listenerDSP = nullptr;

FMOD_DSP_DESCRIPTION ListenerDesc =
{
    FMOD_PLUGIN_SDK_VERSION,
    "FMOD Resonance Audio Listener",    // name.
    0x00010000,                         // plug-in version.
    1,                                  // number of input buffers to process.
    1,                                  // number of output buffers to process.
    createListener,                     // DSP::createDSP callback.
    releaseListener,                    // DSP::release callback.
    resetSource,                        // DSP::reset callback.
    nullptr,                            // called on each DSP update.
    processListener,                    // DSP::process callback.
    nullptr,                            // Channel::setPosition callback.
    0,                                  // number of parameters.
    0,                                  // number of parameters structure.
    nullptr,                            // DSP::setParameterFloat callback.
    nullptr,                            // DSP::setParameterInt  callback.
    nullptr,                            // DSP::setParameterBool callback.
    nullptr,                            // DSP::setParameterData callback.
    nullptr,                            // DSP::getParameterFloat callback.
    nullptr,                            // DSP::getParameterInt callback.
    nullptr,                            // DSP::getParameterBool callback.
    nullptr,                            // DSP::getParameterData callback.
    nullptr,                            // DSP::shouldIProcess callback.
    nullptr,                            // userdata.
    sysRegisterListener,                // called by System::loadPlugin.
    sysDeregisterListener,              // called by System::release.
    nullptr,                            // called when mixer starts/finishes.
};

FMOD_RESULT F_CALLBACK
createListener(FMOD_DSP_STATE* dsp_state)
{
    return FMOD_OK;
}

FMOD_RESULT F_CALLBACK
releaseListener(FMOD_DSP_STATE* dsp_state)
{
    return FMOD_OK;
}

FMOD_RESULT F_CALLBACK
processListener(FMOD_DSP_STATE* dsp_state, unsigned int length, const FMOD_DSP_BUFFER_ARRAY* in_buffer_array, FMOD_DSP_BUFFER_ARRAY* out_buffer_array, FMOD_BOOL inputs_idle, FMOD_DSP_PROCESS_OPERATION operation)
{
    // This callback will be called twice per mix as it has a dual purpose. Once will be with
    // operation = FMOD_DSP_PROCESS_QUERY, and then depending on the return value of the query, if
    // it is FMOD_OK it will call it again with FMOD_DSP_PROCESS_PERFORM.
    if (operation == FMOD_DSP_PROCESS_QUERY) {
        if (out_buffer_array) {
            out_buffer_array->bufferchannelmask[0] = 0;
            out_buffer_array->buffernumchannels[0] = kOutputChannels;
            out_buffer_array->speakermode = FMOD_SPEAKERMODE_STEREO;
        }
        return FMOD_OK;
    }
    
    // renders and mixes buffers (other sound may not be spatialized)
    // warning: we assume that input buffer has kOutputChannels channels number
    ResonanceAudioPlugin::render(in_buffer_array->buffers[0], out_buffer_array->buffers[0], length);
    
//    ResonanceAudioPlugin::render(out_buffer_array->buffers[0], length);
//    // mix buffers (other sound may not be spatialized)
//    if (in_buffer_array && (in_buffer_array->buffernumchannels[0] == kOutputChannels)) {
//        const size_t buffer_size = kOutputChannels * length;
//        ResonanceAudioPlugin::mix(in_buffer_array->buffers[0], out_buffer_array->buffers[0], buffer_size);
//    }
    
    return FMOD_OK;
}

FMOD_RESULT F_CALLBACK
sysRegisterListener(FMOD_DSP_STATE* dsp_state)
{
    return FMOD_OK;
}

FMOD_RESULT F_CALLBACK
sysDeregisterListener(FMOD_DSP_STATE* dsp_state)
{
    // destroy the ResonanceAudio system
    ResonanceAudioPlugin::close();
    
    return FMOD_OK;
}


void addListenerDSP(FMOD::System* system)
{
    FMOD_DSP_DESCRIPTION *desc = ResonanceAudioPlugin_GetFmodDSPDescription();
    FMOD::ChannelGroup *master;
    FMOD_RESULT result;
    
    result = system->createDSP(desc, &listenerDSP);
    FMOD_ERRCHECK(result);
    result = system->getMasterChannelGroup(&master);
    FMOD_ERRCHECK(result);
    result = master->addDSP(FMOD_CHANNELCONTROL_DSP_TAIL, listenerDSP);
    FMOD_ERRCHECK(result);
    
    if (listenerDSP) Log_d("FMOD DSP listener created");
    else  Log_e("FMOD DSP fail to create listener");
}

void removeListenerDSP(FMOD::System* system)
{
    FMOD::ChannelGroup *master;
    FMOD_RESULT result;
    
    if (listenerDSP != nullptr) {
        result = system->getMasterChannelGroup(&master);
        FMOD_ERRCHECK(result);
        result = master->removeDSP(listenerDSP);
        FMOD_ERRCHECK(result);
        
        listenerDSP = nullptr;
        Log_d("FMOD DSP listener removed");
    }
}



// -- Source  --------------------------------------------------------------------------------------

FMOD_DSP_DESCRIPTION SourceDesc =
{
    FMOD_PLUGIN_SDK_VERSION,
    "FMOD Resonance Audio Source",  // name.
    0x00010000,                     // plug-in version.
    1,                              // number of input buffers to process.
    1,                              // number of output buffers to process.
    createSource,                   // DSP::createDSP callback.
    releaseSource,                  // DSP::release callback.
    resetSource,                    // DSP::reset callback.
    nullptr,                        // called on each DSP update.
    processSource,                  // DSP::process callback.
    nullptr,                        // Channel::setPosition callback.
    0,                              // number of parameters.
    0,                              // number of parameters structure.
    nullptr,                        // DSP::setParameterFloat callback.
    nullptr,                        // DSP::setParameterInt  callback.
    nullptr,                        // DSP::setParameterBool callback.
    nullptr,                        // DSP::setParameterData callback.
    nullptr,                        // DSP::getParameterFloat callback.
    nullptr,                        // DSP::getParameterInt callback.
    nullptr,                        // DSP::getParameterBool callback.
    nullptr,                        // DSP::getParameterData callback.
    nullptr,                        // DSP::shouldIProcess callback.
    nullptr,                        // userdata.
    nullptr,                        // called by System::loadPlugin.
    nullptr,                        // called by System::release.
    nullptr,                        // called when mixer starts/finishes.
};


FMOD_RESULT F_CALLBACK
createSource(FMOD_DSP_STATE* dsp_state)
{
    void* userdata;

    FMOD::DSP *dsp = ((FMOD::DSP*)dsp_state->instance);
    FMOD_RESULT result = dsp->getUserData(&userdata);
    FMOD_ERRCHECK(result);
    
    dsp_state->plugindata = reinterpret_cast<SourceState*>(userdata);
    return dsp_state->plugindata == nullptr ? FMOD_ERR_MEMORY : FMOD_OK;
}

FMOD_RESULT F_CALLBACK
releaseSource(FMOD_DSP_STATE* dsp_state)
{
    SourceState* source = reinterpret_cast<SourceState*>(dsp_state->plugindata);
    if (source != nullptr) {
        ResonanceAudioPlugin::destroySource(source->source_id);
        FMOD_DSP_FREE(dsp_state, source);
    }
    return FMOD_OK;
}

FMOD_RESULT F_CALLBACK
resetSource(FMOD_DSP_STATE* dsp_state)
{
    SourceState* source = reinterpret_cast<SourceState*>(dsp_state->plugindata);
    if (source != nullptr) {
        int id = source->source_id;
        dsp_state->plugindata = FMOD_DSP_ALLOC(dsp_state, sizeof(SourceState));
        if (dsp_state->plugindata == nullptr) {
            return FMOD_ERR_MEMORY;
        }

        source = reinterpret_cast<SourceState*>(dsp_state->plugindata);
        source->source_id = id;
    }
    return FMOD_OK;
}

FMOD_RESULT F_CALLBACK
processSource(FMOD_DSP_STATE* dsp_state, unsigned int length, const FMOD_DSP_BUFFER_ARRAY* in_buffer_array, FMOD_DSP_BUFFER_ARRAY* out_buffer_array, FMOD_BOOL inputs_idle, FMOD_DSP_PROCESS_OPERATION operation)
{
//    // This callback will be called twice per mix as it has a dual purpose. Once
//    // will be with operation = FMOD_DSP_PROCESS_QUERY,
//    // and then depending on the return value of the query, if it is FMOD_OK it
//    // will call it again with FMOD_DSP_PROCESS_PERFORM.
//    if (operation == FMOD_DSP_PROCESS_QUERY) {
//        if (out_buffer_array) {
//            out_buffer_array->bufferchannelmask[0] = 0;
//            out_buffer_array->buffernumchannels[0] = kOutputChannels;
//            out_buffer_array->speakermode = FMOD_SPEAKERMODE_STEREO;
//        }
//        return static_cast<bool>(inputs_idle) ? FMOD_ERR_DSP_DONTPROCESS : FMOD_OK;
//    }
//
//    // render
//    if (operation == FMOD_DSP_PROCESS_PERFORM) {
//        //printf("--- DSP numbuffers:%i, buffernumchannels:%i, speakermode: %i\n", in_buffer_array->numbuffers, in_buffer_array->buffernumchannels[0], in_buffer_array->speakermode);
//        SourceState *source = reinterpret_cast<SourceState*>(dsp_state->plugindata);
//        ResonanceAudioPlugin::render(source->source_id, in_buffer_array->buffers[0], out_buffer_array->buffers[0], in_buffer_array->buffernumchannels[0], length);
//    }
//
//    return FMOD_OK;
    
    // This callback will be called twice per mix as it has a dual purpose. Once will be with
    // operation = FMOD_DSP_PROCESS_QUERY, and then depending on the return value of the query, if
    // it is FMOD_OK it will call it again with FMOD_DSP_PROCESS_PERFORM.

    if (operation == FMOD_DSP_PROCESS_QUERY) {
        return static_cast<bool>(inputs_idle) ? FMOD_ERR_DSP_DONTPROCESS : FMOD_OK;
    }
        
    // process the next buffer
    SourceState *source = reinterpret_cast<SourceState*>(dsp_state->plugindata);
    ResonanceAudioPlugin::fill(source->source_id, in_buffer_array->buffers[0], in_buffer_array->buffernumchannels[0], length);
    
    // fill the output buffer with zeros
    if (out_buffer_array) {
        const size_t size = length * out_buffer_array->buffernumchannels[0];
        std::fill_n(out_buffer_array->buffers[0], size, 0.0f);
    }
    
    return FMOD_OK;
}


FMOD::DSP* createSourceDSP(FMOD::System* system, void* userdata)
{
    FMOD::DSP *dsp;
    
    // get the DSP description for source and store the source state as user data
    FMOD_DSP_DESCRIPTION *desc = ResonanceAudioSource_GetFmodDSPDescription();
    desc->userdata = userdata;
    
    FMOD_RESULT result = system->createDSP(desc, &dsp);
    FMOD_ERRCHECK(result);
    
    if (dsp) Log_d("FMOD DSP create source with id: %i", ((SourceState*)userdata)->source_id);
    else Log_e("FMOD DSP fail to create source for id: %i", ((SourceState*)userdata)->source_id);
    
    return dsp;
}

void addSourceDSP(FMOD::Channel* channel, FMOD::DSP* dsp)
{
    void* userdata;
    FMOD_RESULT result = dsp->getUserData(&userdata);
    FMOD_ERRCHECK(result);
    
    result = channel->addDSP(FMOD_CHANNELCONTROL_DSP_FADER, dsp);
    FMOD_ERRCHECK(result);
    
    if (result == FMOD_OK) Log_d("FMOD DSP source added: %i", ((SourceState*)userdata)->source_id);
    else Log_e("FMOD DSP fail to add source for id: %i", ((SourceState*)userdata)->source_id);
}

void removeSourceDSP(FMOD::Channel* channel, FMOD::DSP* dsp)
{
    void* userdata;
    FMOD_RESULT result = dsp->getUserData(&userdata);
    FMOD_ERRCHECK(result);
    
    result = channel->removeDSP(dsp);
    FMOD_ERRCHECK(result);
    
    if (result == FMOD_OK) Log_d("FMOD DSP source removed: %i", ((SourceState*)userdata)->source_id);
    else Log_e("FMOD DSP fail to remove source for id: %i", ((SourceState*)userdata)->source_id);
}

void releaseSourceDSP(FMOD::DSP* dsp)
{
    if (dsp != nullptr) {
        void* userdata;
        FMOD_RESULT result = dsp->getUserData(&userdata);
        FMOD_ERRCHECK(result);
        
        Log_d("FMOD DSP release source: %i", ((SourceState*)userdata)->source_id);
        ResonanceAudioPlugin::destroySource(((SourceState*)userdata)->source_id);
    }
}



//static FMOD_PLUGINLIST plugin_list[] = {{FMOD_PLUGINTYPE_DSP, &ResonanceAudioPlugin::DSP::fmod::dspdesc}, {FMOD_PLUGINTYPE_MAX, nullptr}};
//
//F_EXPORT FMOD_PLUGINLIST* F_CALL getDSPDescription()
//{
//    // Listener plugin parameters.
//    return plugin_list;
//}

}
#endif // CORDOVA_PLUGIN_FMOD
}
}


#if CORDOVA_PLUGIN_FMOD
F_EXPORT FMOD_DSP_DESCRIPTION* F_CALL
ResonanceAudioPlugin_GetFmodDSPDescription()
{
    return &ResonanceAudioPlugin::DSP::fmod::ListenerDesc;
}
F_EXPORT FMOD_DSP_DESCRIPTION* F_CALL
ResonanceAudioSource_GetFmodDSPDescription()
{
    return &ResonanceAudioPlugin::DSP::fmod::SourceDesc;
}
#endif
