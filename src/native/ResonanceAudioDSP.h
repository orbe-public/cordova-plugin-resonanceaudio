// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once

#include <stddef.h>

#if CORDOVA_PLUGIN_FMOD
#include "FMODPlugin.h"
#endif




namespace ResonanceAudioPlugin {

// -------------------------------------------------------------------------------------------------
    #pragma mark DSP
// -------------------------------------------------------------------------------------------------

namespace DSP {
#if CORDOVA_PLUGIN_FMOD
namespace fmod {


    void addListenerDSP(FMOD::System *system);
    void removeListenerDSP(FMOD::System *system);

    // callback to be called on creation of a listener instance
    FMOD_RESULT F_CALLBACK createListener(FMOD_DSP_STATE* dsp_state);
    // callback to be called on release of a listener instance
    FMOD_RESULT F_CALLBACK releaseListener(FMOD_DSP_STATE* dsp_state);
    // callback to be called to process the listener output buffer
    FMOD_RESULT F_CALLBACK processListener(FMOD_DSP_STATE* dsp_state,
                                   unsigned int length,
                                   const FMOD_DSP_BUFFER_ARRAY* in_buffer_array,
                                   FMOD_DSP_BUFFER_ARRAY* out_buffer_array,
                                   FMOD_BOOL inputs_idle,
                                   FMOD_DSP_PROCESS_OPERATION operation);

    // callback to be called on registration of the listener
    FMOD_RESULT F_CALLBACK sysRegisterListener(FMOD_DSP_STATE* dsp_state);
    // callback to be called on deregistration of the listener
    FMOD_RESULT F_CALLBACK sysDeregisterListener(FMOD_DSP_STATE* dsp_state);



    struct SourceState {
        SourceState(int id) : source_id(id) {}
        int source_id; // the sound object source ID
    };

    FMOD::DSP* createSourceDSP(FMOD::System *system, void* userdata);
    void addSourceDSP(FMOD::Channel *channel, FMOD::DSP *dsp);
    void removeSourceDSP(FMOD::Channel *channel, FMOD::DSP *dsp);
    void releaseSourceDSP(FMOD::DSP* dsp);

    // callback to be called on creation of a source instance
    FMOD_RESULT F_CALLBACK createSource(FMOD_DSP_STATE* dsp_state);
    // callback to be called on release of a source instance
    FMOD_RESULT F_CALLBACK releaseSource(FMOD_DSP_STATE* dsp_state);
    // callback to be called when a source is reset
    FMOD_RESULT F_CALLBACK resetSource(FMOD_DSP_STATE* dsp_state);
    // callback to be called to process the source output buffer
    FMOD_RESULT F_CALLBACK processSource(FMOD_DSP_STATE* dsp_state,
                                   unsigned int length,
                                   const FMOD_DSP_BUFFER_ARRAY* in_buffer_array,
                                   FMOD_DSP_BUFFER_ARRAY* out_buffer_array,
                                   FMOD_BOOL inputs_idle,
                                   FMOD_DSP_PROCESS_OPERATION operation);



    // returns an FMOD_PLUGINLIST containing pointers to the DSP plugin descriptions
//    extern "C" F_EXPORT FMOD_PLUGINLIST* F_CALL getDSPDescription();
    
} // namespace fmod
#endif
} // namespace DSP
} // namespace ResonanceAudioPlugin


extern "C"
{
    #if CORDOVA_PLUGIN_FMOD
    // returns DSPs description
    F_EXPORT FMOD_DSP_DESCRIPTION* F_CALL ResonanceAudioPlugin_GetFmodDSPDescription();
    F_EXPORT FMOD_DSP_DESCRIPTION* F_CALL ResonanceAudioSource_GetFmodDSPDescription();
    #endif
}
