// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <algorithm>
#include <map>

#include "ResonanceAudioPlugin.h"
#include "room_effects_utils.h"
#include "utils.h"

#if CORDOVA_PLUGIN_FMOD
#include "FMODPlugin.h"
#endif





namespace ResonanceAudioPlugin {

// -------------------------------------------------------------------------------------------------
// log
// -------------------------------------------------------------------------------------------------

void Log_d( const char* format, ... )
{
    char msg[1024];
    va_list args;

    va_start( args, format );
    vsprintf( msg, format, args );
    va_end( args );

    #ifdef __ANDROID__
    #if DEBUG
    __android_log_write( ANDROID_LOG_INFO, "Resonance Audio plugin", msg );
    #endif
    #else
    NSLog_d(msg);
    #endif
}

void Log_w( const char* format, ... )
{
    char msg[1024];
    va_list args;

    va_start( args, format );
    vsprintf( msg, format, args );
    va_end( args );

    #ifdef __ANDROID__
    __android_log_write( ANDROID_LOG_WARN, "Resonance Audio plugin", msg );
    #else
    NSLog_w(msg);
    #endif
}

void Log_e( const char* format, ... )
{
    char msg[1024];
    va_list args;

    va_start( args, format );
    vsprintf( msg, format, args );
    va_end( args );

    #ifdef __ANDROID__
    __android_log_write( ANDROID_LOG_ERROR, "Resonance Audio plugin", msg );
    #else
    NSLog_e(msg);
    #endif
}



// -------------------------------------------------------------------------------------------------
// log
// -------------------------------------------------------------------------------------------------

// The current audio context
void *audiocontext = nullptr;
// ResonanceAudio API pointer (must be unique)
std::unique_ptr<ResonanceAudioApi> api = nullptr;
// the current scene
Scene *scene = nullptr;
// the current listener
Listener *listener = nullptr;
// List of all current sources
std::map<ResonanceAudioApi::SourceId,Source*> sources;



void initialize(void *context)
{
    if (scene == nullptr) {
        scene = new Scene();
        listener = new Listener();
        #if CORDOVA_PLUGIN_FMOD
        if (context!=nullptr) {
            reinterpret_cast<FMODPlugin::Context*>(context)->addResonanceAudioPlugin();
            audiocontext = context;
        }
        #endif
        Log_d("Resonance Audio environment inited");
    }
    else Log_w("Resonance Audio environment already inited");
}

void close()
{
    for(auto it:sources) destroySource(it.first);
    sources.clear();
    
    #if CORDOVA_PLUGIN_FMOD
    if (audiocontext!=nullptr) {
        reinterpret_cast<FMODPlugin::Context*>(audiocontext)->removeResonanceAudioPlugin();
        audiocontext = nullptr;
    }
    #endif
}

//System* createSystem(int sample_rate_hz, size_t num_channels, size_t frames_per_buffer)
//{
//    System *system = new System(sample_rate_hz, num_channels, frames_per_buffer);
//    api = std::unique_ptr<ResonanceAudioApi>(vraudio::CreateResonanceAudioApi(num_channels, frames_per_buffer, sample_rate_hz));
//
//    return system;
//}

void createSystem(int sample_rate_hz, size_t num_channels, size_t frames_per_buffer)
{
    api = std::unique_ptr<ResonanceAudioApi>(vraudio::CreateResonanceAudioApi(num_channels, frames_per_buffer, sample_rate_hz));
    
    Log_d("... CREATE API sample_rate_hz:%i, num_channels: %i, frames_per_buffer: %i", sample_rate_hz, num_channels, frames_per_buffer);
}

void enableRoomEffects(bool enable)
{
    if (api) api->EnableRoomEffects(enable);
}


ResonanceAudioApi::SourceId createSource(size_t num_channels)
{
    // TODO: expose rendering mode as second argument
    ResonanceAudioApi::SourceId id = api->CreateSoundObjectSource(scene->renderingmode);
    
    // updates distance model to ensure near field effects are only applied when the minimum distance
    // is below 1m. The +1.0f here ensures that max distance is greater than min distance.
    float kNearFieldThreshold = 1.0f;
    api->SetSourceDistanceModel(id, DistanceRolloffModel::kNone, kNearFieldThreshold, kNearFieldThreshold+1.0f);
    api->SetSourcePosition(id, 0, 0, 0);
    
    sources[id] = new Source(id);
    Log_d("... create new source: id: %i, sources: %i - rendering mode: %i", id, sources.size(), scene->renderingmode);
    
    return id;
}

void destroySource(ResonanceAudioApi::SourceId id)
{
    api->DestroySource(id);
    auto it = sources.find(id);
    if (it != sources.end()) sources.erase(it);
    
    Log_d("... SOURCE DESTROYED %i, sources: %i", id, sources.size());
}


bool setRoomProperties(RoomProperties properties)
{
    if (scene == nullptr) return false;
    
    scene->room = properties;
    
    // update reflection and revert
    api->SetReflectionProperties(ComputeReflectionProperties(scene->room));
    api->SetReverbProperties(ComputeReverbProperties(scene->room));
    
    Log_d("... update room properties width:%f, height:%f, depth:%f", properties.dimensions[0], properties.dimensions[1], properties.dimensions[2]);
    
    return true;
}

//bool setRoomProperties(float dimensions[], MaterialName materials[])
//{
//    if (scene == nullptr) return false;
//    
//    scene->room.dimensions[0] = dimensions[0];
//    scene->room.dimensions[1] = dimensions[1];
//    scene->room.dimensions[2] = dimensions[2];
//    scene->room.material_names[0] = materials[0];
//    scene->room.material_names[1] = materials[1];
//    scene->room.material_names[2] = materials[2];
//    scene->room.material_names[3] = materials[3];
//    scene->room.material_names[4] = materials[4];
//    scene->room.material_names[5] = materials[5];
//    return true;
//}
//bool setSpeedOfSound(float speed)
//{
//    if (scene == nullptr) return false;
//    
//    scene->sound_speed = speed;
//    return  true;
//}

bool setSourceRenderingMode(int mode)
{
    if (scene == nullptr) return false;
    
    switch (mode) {
        case 2:
            scene->renderingmode = RenderingMode::kBinauralMediumQuality;
            break;
        case 3:
            scene->renderingmode = RenderingMode::kBinauralHighQuality;
            break;
        default:
            scene->renderingmode = RenderingMode::kBinauralLowQuality;
            break;
    }

    return  true;
}

Eigen::Matrix4f getMatrix(float position[3], float forward[3], float up[3])
{
    const Eigen::Vector3f vposition(position[0], position[1], position[2]);
    const Eigen::Vector3f vforward(forward[0], forward[1], forward[2]);
    const Eigen::Vector3f vup(up[0], up[1], up[2]);
    
    return GetTransformMatrix(vposition, vforward, vup);
}

bool setListenerPosition(float x, float y, float z)
{
    if (listener == nullptr) return false;
    
    listener->position[0] = x;
    listener->position[1] = y;
    listener->position[2] = z;
    
    Eigen::Matrix4f transform = getMatrix(listener->position, listener->forward, listener->up);
    const Eigen::Vector3f pos = GetPosition(transform);
    
    api->SetHeadPosition(pos.x(), pos.y(), pos.z());
    //Log_d("... set head position x:%f -> %f, y:%f -> %f, z:%f -> %f", x, pos.x(), y, pos.y(), z, pos.z());
    
    return true;
}

bool setListenerOrientation(float forwardX, float forwardY, float forwardZ, float upX, float upY, float upZ)
{
    if (listener == nullptr) return false;
    
    listener->forward[0] = forwardX;
    listener->forward[1] = forwardY;
    listener->forward[2] = forwardZ;
    listener->up[0] = upX;
    listener->up[1] = upY;
    listener->up[2] = upZ;
    
    Eigen::Matrix4f transform = getMatrix(listener->position, listener->forward, listener->up);
    const Eigen::Quaternionf q = GetQuaternion(transform);
    
    api->SetHeadRotation(q.x(), q.y(), q.z(), q.w());
    //Log_d("... set head orientation x:%f, y:%f, z:%f, w:%f", q.x(), q.y(), q.z(), q.w());
    
    return true;
}

void setSourcePosition(ResonanceAudioApi::SourceId id, float x, float y, float z)
{
    if (id == api->kInvalidSourceId || !sources[id]) return;
    
    sources[id]->position[0] = x;
    sources[id]->position[1] = y;
    sources[id]->position[2] = z;
    
    Eigen::Matrix4f transform = getMatrix(sources[id]->position, sources[id]->forward, sources[id]->up);
    //FlipZAxis(&transform);
    
    const WorldPosition pos = GetPosition(transform);
    api->SetSourcePosition(id, pos.x(), pos.y(), pos.z());
    
    
    // update the distance attenuation with respect to the distance between the source and the listener.
    //sources[id]->distance = 1.0f / FastReciprocalSqrt(pos.x()*pos.x() +pos.y()*pos.y() + pos.z()*pos.z());
    
    // calculates the gain value for the source position with respect to the given room properties.
    const WorldPosition room_pos = WorldPosition(scene->room.position);
    const WorldRotation room_rot = WorldRotation(scene->room.rotation);
    const WorldPosition room_dim = WorldPosition(scene->room.dimensions);
    float room_gain = ComputeRoomEffectsGain(pos, room_pos, room_rot, room_dim);
    api->SetSourceRoomEffectsGain(id, room_gain);
    
    //Log_d("... set source(%i) position x:%f, y:%f, z:%f - distance: %f, room gain: %f", id, x, y , z, sources[id]->distance, room_gain);
}

void setSourceOrientation(ResonanceAudioApi::SourceId id, float forwardX, float forwardY, float forwardZ, float upX, float upY, float upZ)
{
    if (id == api->kInvalidSourceId && !sources[id]) return;
    
    sources[id]->forward[0] = forwardX;
    sources[id]->forward[1] = forwardY;
    sources[id]->forward[2] = forwardZ;
    sources[id]->up[0] = upX;
    sources[id]->up[1] = upY;
    sources[id]->up[2] = upZ;
    
    Eigen::Matrix4f transform = getMatrix(sources[id]->position, sources[id]->forward, sources[id]->up);
    //FlipZAxis(&transform);
    
    const Eigen::Quaternionf q = GetQuaternion(transform);
    api->SetSourceRotation(id, q.x(), q.y(), q.z(), q.w());
    //Log_d("... set source(%i) orientation x:%f, y:%f, z:%f, w:%f", id, q.x(), q.y(), q.z(), q.w());
}

void setSourceDistanceModel(ResonanceAudioApi::SourceId id, DistanceRolloffModel model, float min_distance, float max_distance)
{
    if (id == api->kInvalidSourceId && !sources[id]) return;
    
    sources[id]->rolloff_model = model;
    sources[id]->min_distance  = min_distance;
    sources[id]->max_distance  = max_distance;
    
    api->SetSourceDistanceModel(id, model, min_distance, max_distance);
    
    Log_d("... set source(%i) distance model: %i, min:%f, max:%f", id, model, min_distance, max_distance);
}

void setSourceRolloffModel(ResonanceAudioApi::SourceId id, DistanceRolloffModel model)
{
    if (sources[id]) setSourceDistanceModel(id, model, sources[id]->min_distance, sources[id]->max_distance);
}

void setSourceMinDistance(ResonanceAudioApi::SourceId id, float min_distance)
{
    if (sources[id]) setSourceDistanceModel(id, sources[id]->rolloff_model, min_distance, sources[id]->max_distance);
}

void setSourceMaxDistance(ResonanceAudioApi::SourceId id, float max_distance)
{
    if (sources[id]) setSourceDistanceModel(id, sources[id]->rolloff_model, sources[id]->min_distance, max_distance);
}

void setSourceRoomEffectsGain(SourceId id, float gain)
{
    api->SetSourceRoomEffectsGain(id, gain);
}

void setSourceVolume(ResonanceAudioApi::SourceId id, float volume)
{
    api->SetSourceVolume(id, volume);
}

void setSourceDirectivityPattern(ResonanceAudioApi::SourceId id, float alpha, float sharpness)
{
    api->SetSoundObjectDirectivity(id, alpha, sharpness);
}

void setSourceSpread(ResonanceAudioApi::SourceId id, float degree)
{
    api->SetSoundObjectSpread(id, degree);
}

void setSourceNearEffectGain(ResonanceAudioApi::SourceId id, float gain)
{
    api->SetSoundObjectNearFieldEffectGain(id, gain);
}

void setSourceOcclusionIntensity(ResonanceAudioApi::SourceId id, float intensity)
{
    api->SetSoundObjectOcclusionIntensity(id, intensity);
}





// Mixes |input| into |input_output|.
//
// @param input Buffer of input data.
// @param input_output Buffer of input data, will contain mixed data on return.
// @param size Length of the two buffers in samples.
void mix(const float* input, float* output, size_t length)
{
    for (size_t sample = 0; sample < length; ++sample) {
        output[sample] += input[sample];
    }
}

// sets the next audio buffer
void fill(ResonanceAudioApi::SourceId id, const float* input, int num_channels, size_t length)
{
    // process with current source
    //Log_d("... fill buffer source(%i)", id);
//    string data = "";
//    for (int i=0; i<10; i++) data += std::to_string(input[0]);
//    Log_d("... fill buffer source(%i): %s", id, data.c_str());
    
    api->SetInterleavedBuffer(id, input, num_channels, length);
}

void render(const float* input, float* output, size_t length)
{
    const size_t buffer_size = kOutputChannels * length;
    if (!api->FillInterleavedOutputBuffer(kOutputChannels, length, output)) {
        // no valid output was rendered, fill the output buffer with zeros
        std::fill_n(output, buffer_size, 0.0f);
        //Log_d("no valid output was rendered");
    }
    
//    string data = "";
//    for (int i=0; i<10; i++) data += std::to_string(output[0]);
//    Log_d("... render output buffer: %s",  data.c_str());
    
    // mix buffers
    mix(input, output, buffer_size);
}


//void render(System* system, const float* input, float* output, size_t length)
//{
//    // update room properties.
//    if (scene != nullptr) {
//        system->api->SetReflectionProperties(scene->reflection);
//        system->api->SetReverbProperties(scene->reverberation);
//    }
//
//    // update head properties
//    if (listener != nullptr) {
//        system->api->SetHeadPosition(listener->position[0], listener->position[1], listener->position[2]);
//        system->api->SetHeadRotation(listener->rotation[0], listener->rotation[1], listener->rotation[2],listener->rotation[3]);
//    }
//
//
//    const size_t buffer_size = kOutputChannels * length;
//    if (!system->api->FillInterleavedOutputBuffer(kOutputChannels, length, output)) {
//        // no valid output was rendered, fill the output buffer with zeros.
//        std::fill_n(output, buffer_size, 0.0f);
//        //Log_d("no valid output was rendered");
//    }
//
//    // mix buffers
//    mix(input, output, buffer_size);
//}

} // namespace ResonanceAudioPlugin
