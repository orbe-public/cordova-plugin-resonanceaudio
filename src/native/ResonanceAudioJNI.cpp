// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <jni.h>
#include <stdio.h>
#include <string>
#include <map>


#define STATUS_OK  1
#define STATUS_ERR 0
#define STATUS_NOTFOUND -1

#include "ResonanceAudioPlugin.h"
#if CORDOVA_PLUGIN_FMOD
#include "FMODJNI.h"
#endif




// The current audio context name
std::string audiocontext = "";

// List of ResonanceAudio sources stored by their target audio source (i.e. player uuid)
//map<std::string, ResonanceAudioApi::SourceId> sources;

MaterialName getMaterialNameFrom(const char* name)
{
    if (strcmp ("transparent", name) == 0) return kTransparent;
    if (strcmp ("acoustic-ceiling-tiles", name) == 0) return kAcousticCeilingTiles;
    if (strcmp ("brick-bare", name) == 0) return kBrickBare;
    if (strcmp ("brick-painted", name) == 0) return kBrickPainted;
    if (strcmp ("concrete-block-coarse", name) == 0) return kConcreteBlockCoarse;
    if (strcmp ("concrete-block-painted", name) == 0) return kConcreteBlockPainted;
    if (strcmp ("curtain-heavy", name) == 0) return kCurtainHeavy;
    if (strcmp ("fiber-glass-insulation", name) == 0) return kFiberGlassInsulation;
    if (strcmp ("glass-thin", name) == 0) return kGlassThin;
    if (strcmp ("glass-thick", name) == 0) return kGlassThick;
    if (strcmp ("glass", name) == 0) return kGrass;
    if (strcmp ("linoleum-on-concrete", name) == 0) return kLinoleumOnConcrete;
    if (strcmp ("marble", name) == 0) return kMarble;
    if (strcmp ("metal", name) == 0) return kMetal;
    if (strcmp ("parquet-on-concrete", name) == 0) return kParquetOnConcrete;
    if (strcmp ("plaster-rough", name) == 0) return kPlasterRough;
    if (strcmp ("plaster-smooth", name) == 0) return kPlasterSmooth;
    if (strcmp ("plywood-panel", name) == 0) return kPlywoodPanel;
    if (strcmp ("polished-concrete-or-tile", name) == 0) return kPolishedConcreteOrTile;
    if (strcmp ("sheetrock", name) == 0) return kSheetrock;
    if (strcmp ("water-or-ice-surface", name) == 0) return kWaterOrIceSurface;
    if (strcmp ("wood-ceiling", name) == 0) return kWoodCeiling;
    if (strcmp ("wood-panel", name) == 0) return kWoodPanel;
    if (strcmp ("uniform", name) == 0) return kUniform;

    return kTransparent;
}

DistanceRolloffModel getRollofModel(const char* model)
{
    if (strcmp ("logarithmic", model) == 0) return DistanceRolloffModel::kLogarithmic;
    if (strcmp ("linear", model) == 0) return DistanceRolloffModel::kLinear;

    return DistanceRolloffModel::kNone;
}


extern "C"
{
    /**
     * ResonanceAudio
     */
    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_resonanceaudio_ResonanceAudio_init(JNIEnv *env, jobject obj, jstring ctx)
    {
        audiocontext = string(env->GetStringUTFChars(ctx, 0));

        #if CORDOVA_PLUGIN_FMOD
        if (audiocontext.compare("fmod") == 0) {
            FMODPlugin::Context *context = FMOD_init_system();
            ResonanceAudioPlugin::initialize(context);
            return STATUS_OK;
        }
        #endif

        return STATUS_NOTFOUND;
    }

    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_resonanceaudio_ResonanceAudio_close(JNIEnv *env, jobject obj)
    {
        ResonanceAudioPlugin::close();
    }

    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_resonanceaudio_ResonanceAudio_enableRoomEffects(JNIEnv *env, jobject obj, jboolean enable)
    {
        ResonanceAudioPlugin::enableRoomEffects((enable==JNI_TRUE));
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_resonanceaudio_ResonanceAudio_setRoomProperties(JNIEnv *env, jobject obj, jfloatArray dimensions, jobjectArray materials)
    {
        vraudio::RoomProperties room;

        // list dimensions
        jfloat* dims = env->GetFloatArrayElements(dimensions, 0);
        for (int i=0, len=env->GetArrayLength(dimensions); i<len; i++) {
            room.dimensions[i] = dims[i];
        }
        env->ReleaseFloatArrayElements(dimensions, dims, 0);

        // convert material names
        for (int i=0, len=env->GetArrayLength(materials); i<len; i++) {
            jstring string = (jstring) (env->GetObjectArrayElement(materials, i));
            const char *name = env->GetStringUTFChars(string, 0);
            room.material_names[i] = getMaterialNameFrom(name);
            env->ReleaseStringUTFChars(string, name);
        }

        if (ResonanceAudioPlugin::setRoomProperties(room)) return STATUS_OK;
        else STATUS_ERR;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_resonanceaudio_ResonanceAudio_setSpeedOfSound(JNIEnv *env, jobject obj, jfloat value)
    {
        //ResonanceAudioPlugin::setSpeedOfSound(value);
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_resonanceaudio_ResonanceAudio_setAmbisonicOrder(JNIEnv *env, jobject obj, jint order)
    {
        if (ResonanceAudioPlugin::setSourceRenderingMode(order)) return STATUS_OK;
        else STATUS_ERR;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_resonanceaudio_ResonanceAudio_setListenerPosition(JNIEnv *env, jobject obj, jfloat x, jfloat y, jfloat z)
    {
        if (ResonanceAudioPlugin::setListenerPosition(x, y, z)) return STATUS_OK;
        else STATUS_ERR;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_resonanceaudio_ResonanceAudio_setListenerOrientation(JNIEnv *env, jobject obj, jfloat fx, jfloat fy, jfloat fz, jfloat ux, jfloat uy, jfloat uz)
    {
        if (ResonanceAudioPlugin::setListenerOrientation(fx, fy, fz, ux, uy, uz)) return STATUS_OK;
        else STATUS_ERR;
    }

    /**
     * @returns an integer where numbers equal to or greater than zero are the ResonanceAudio source
     * ID and numbers less than zero are an error (see below):
     *
     * >=0 = source id
     * -1 = invalid source id
     * -2 = player not found
     * -3 = missing source or not loaded
     * -4 = invalid or missing audio context
     */
    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_resonanceaudio_ResonanceAudio_createSource(JNIEnv *env, jobject obj, jstring uuid, jfloat px, jfloat py, jfloat pz, jfloat min, jfloat max, jstring rollof, jfloat gain, jfloat fx, jfloat fy, jfloat fz, jfloat ux, jfloat uy, jfloat uz, jfloat alpha, jfloat sharpness, jfloat sourceWidth)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        const char *name = env->GetStringUTFChars(rollof, 0);

        // channels = -1 means unconfigured audio source
        int channels = -1;

        // using FMOD as audio context
        #if CORDOVA_PLUGIN_FMOD
        FMODPlugin::Player* player = NULL;
        if (audiocontext.compare("fmod") == 0) {
            player = FMOD_get_player(id);
            if (player) channels = player->getChannels();
            else return -2;
        }
        #endif

        // the audio source exists and well configured
        if (channels>0) {
            ResonanceAudioApi::SourceId sid = ResonanceAudioPlugin::createSource(channels);
            DistanceRolloffModel model = getRollofModel(name);
            if (sid != ResonanceAudioApi::kInvalidSourceId) {
                //sources[id] = sid;
                ResonanceAudioPlugin::setSourcePosition(sid, px, py, pz);
                ResonanceAudioPlugin::setSourceOrientation(sid, fx, fy, fz, ux, uy, uz);
                ResonanceAudioPlugin::setSourceDistanceModel(sid, model, min, max);
                ResonanceAudioPlugin::setSourceDirectivityPattern(sid, alpha, sharpness);
                ResonanceAudioPlugin::setSourceSpread(sid, sourceWidth);
                ResonanceAudioPlugin::setSourceVolume(sid, gain);

                #if CORDOVA_PLUGIN_FMOD
                if (audiocontext.compare("fmod") == 0) player->createResonanceAudioDSP(sid);
                #endif

                return sid;
            }
            else return -1;
        }

        // attempts to define the correct error
        if (!audiocontext.empty()) {
            if (audiocontext.compare("fmod") == 0) return -3;
        }

        return -4;
    }



    /**
     * ResonanceAudioSource
     */
    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_resonanceaudio_ResonanceAudioSource_setPosition(JNIEnv *env, jobject obj, jint uuid, jfloat x, jfloat y, jfloat z)
    {
        ResonanceAudioPlugin::setSourcePosition(uuid, x, y, z);
    }

    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_resonanceaudio_ResonanceAudioSource_setOrientation(JNIEnv *env, jobject obj, jint uuid, jfloat fx, jfloat fy, jfloat fz, jfloat ux, jfloat uy, jfloat uz)
    {
        ResonanceAudioPlugin::setSourceOrientation(uuid, fx, fy, fz, ux, uy, uz);
    }

    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_resonanceaudio_ResonanceAudioSource_setMinDistance(JNIEnv *env, jobject obj, jint uuid, jfloat distance)
    {
        ResonanceAudioPlugin::setSourceMinDistance(uuid, distance);
    }

    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_resonanceaudio_ResonanceAudioSource_setMaxDistance(JNIEnv *env, jobject obj, jint uuid, jfloat distance)
    {
        ResonanceAudioPlugin::setSourceMaxDistance(uuid, distance);
    }

    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_resonanceaudio_ResonanceAudioSource_setRolloff(JNIEnv *env, jobject obj, jint uuid, jstring rollof)
    {
        const char *name = env->GetStringUTFChars(rollof, 0);
        DistanceRolloffModel model = getRollofModel(name);
        ResonanceAudioPlugin::setSourceRolloffModel(uuid, model);
    }

    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_resonanceaudio_ResonanceAudioSource_setDirectivityPattern(JNIEnv *env, jobject obj, jint uuid, jfloat alpha, jfloat sharpness)
    {
        ResonanceAudioPlugin::setSourceDirectivityPattern(uuid, alpha, sharpness);
    }

    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_resonanceaudio_ResonanceAudioSource_setSourceWidth(JNIEnv *env, jobject obj, jint uuid, jfloat width)
    {
        ResonanceAudioPlugin::setSourceSpread(uuid, width);
    }

    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_resonanceaudio_ResonanceAudioSource_setSourceNearEffectGain(JNIEnv *env, jobject obj, jint uuid, jfloat gain)
    {
        ResonanceAudioPlugin::setSourceNearEffectGain(uuid, gain);
    }

    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_resonanceaudio_ResonanceAudioSource_setSoundOcclusionIntensity(JNIEnv *env, jobject obj, jint uuid, jfloat intensity)
    {
        ResonanceAudioPlugin::setSourceOcclusionIntensity(uuid, intensity);
    }
}