// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


#import "CDVResonanceAudio.h"
#include "ResonanceAudioPlugin.h"

#if CORDOVA_PLUGIN_FMOD
#import "CDVFMOD.h"
#import "CDVFMODPlayer.h"
#endif





static CDVResonanceAudio *resonanceaudio;

@interface CDVResonanceAudio()
@end

@implementation CDVResonanceAudio
{
    // list of audio sources
    //@private map<string, ResonanceAudioApi::SourceId> sources;
    // required audio context
    @private NSString* context;
}



- (void)pluginInitialize
{
    resonanceaudio = self;
    context = nil;
    
    NSLog(@"Resonance Audio plugin initialized");
}

- (void)onAppTerminate
{
    // cleanup on app exit
}



//+ (void)soundEnded:(NSString*)uuid
//{
//    const char* pid = [uuid UTF8String];
//    if (resonanceaudio->sources.find(pid) != resonanceaudio->sources.end()) {
//        ResonanceAudioApi::SourceId sid = resonanceaudio->sources[pid];
//        ResonanceAudioPlugin::destroySource(sid);
//    }
//}

- (void)init:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    context = [command argumentAtIndex:0];
    NSDictionary *options = [command argumentAtIndex:1]; /// << ----------- TODO
    
    if ( context!=nil ) {
        #if CORDOVA_PLUGIN_FMOD
        if ([context isEqual:@"fmod"]) {
            ResonanceAudioPlugin::initialize([CDVFMOD initFMODSystem]);
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        }
        #endif
    }
    
    // invalid or missing audio context
    if (pluginResult==nil) {
        NSString *message = [NSString stringWithFormat:@"invalid or missing audio context '%@'", context];
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                         messageAsString:message];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)close:(CDVInvokedUrlCommand*)command
{
    #if CORDOVA_PLUGIN_FMOD
    ResonanceAudioPlugin::close();
    #endif
    
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK]
                                callbackId:command.callbackId];
}

- (void)enableRoomEffects:(CDVInvokedUrlCommand*)command
{
    NSNumber *enable = [command argumentAtIndex:0];
    ResonanceAudioPlugin::enableRoomEffects([enable boolValue]);
    
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK]
                                callbackId:command.callbackId];
}

- (void)createSource:(CDVInvokedUrlCommand*)command
{
    //[self.commandDelegate runInBackground:^{
        CDVPluginResult *pluginResult = nil;
        NSDictionary *arg = [command argumentAtIndex:0 withDefault:nil andClass:[NSDictionary class]];
        NSString *identifier = arg==nil ? [command argumentAtIndex:0] : [arg valueForKey:@"uuid"];
        NSDictionary *options = [command argumentAtIndex:1];
        
        // parse options
        NSArray *position   = [options valueForKey:@"position"];
        NSArray *forward    = [options valueForKey:@"forward"];
        NSArray *up         = [options valueForKey:@"up"];
        NSString *rolloff   = [options valueForKey:@"rolloff"];
        NSNumber *mindist   = [options valueForKey:@"minDistance"];
        NSNumber *maxdist   = [options valueForKey:@"maxDistance"];
        NSNumber *gain      = [options valueForKey:@"gain"];
        NSNumber *alpha     = [options valueForKey:@"alpha"];
        NSNumber *sharpness = [options valueForKey:@"sharpness"];
        NSNumber *width     = [options valueForKey:@"sourceWidth"];
        
        float px = [position[0] floatValue];
        float py = [position[1] floatValue];
        float pz = [position[2] floatValue];
        float fx = [forward[0] floatValue];
        float fy = [forward[1] floatValue];
        float fz = [forward[2] floatValue];
        float ux = [up[0] floatValue];
        float uy = [up[1] floatValue];
        float uz = [up[2] floatValue];
        
        DistanceRolloffModel model = DistanceRolloffModel::kNone;
        if ([rolloff isEqualToString:@"logarithmic"]) model = DistanceRolloffModel::kLogarithmic;
        if ([rolloff isEqualToString:@"linear"]) model = DistanceRolloffModel::kLinear;

    
        // channels = -1 means unconfigured audio source
        int channels = -1;
    
        // using FMOD as audio context
        #if CORDOVA_PLUGIN_FMOD
        FMODPlugin::Player* player = NULL;
        if (context!=nil && [context isEqual:@"fmod"]) {
            player = [CDVFMODPlayer getPlayer:identifier];
            if (player) channels = player->getChannels();
        }
        #endif
        
        // the audio source exists and well configured
        if (channels>0) {
            ResonanceAudioApi::SourceId sid = ResonanceAudioPlugin::createSource(channels);
            if (sid != ResonanceAudioApi::kInvalidSourceId) {
                string pid = [identifier UTF8String];
                //resonanceaudio->sources[pid] = sid;
                ResonanceAudioPlugin::setSourcePosition(sid, px, py, pz);
                ResonanceAudioPlugin::setSourceOrientation(sid, fx, fy, fz, ux, uy, uz);
                ResonanceAudioPlugin::setSourceDistanceModel(sid, model, [mindist floatValue], [maxdist floatValue]);
                ResonanceAudioPlugin::setSourceDirectivityPattern(sid, [alpha floatValue], [sharpness floatValue]);
                ResonanceAudioPlugin::setSourceSpread(sid, [width floatValue]);
                ResonanceAudioPlugin::setSourceVolume(sid, [gain floatValue]);
                
                #if CORDOVA_PLUGIN_FMOD
                if (context!=nil && [context isEqual:@"fmod"]) player->createResonanceAudioDSP(sid);
                #endif
                
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsInt:sid];
            }
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                  messageAsString:@"invalid source id"];
        }
        else if (context!=nil) {
            // FMOD audio context
            if ([context isEqual:@"fmod"]) {
                NSString *message = channels == -1 ? @"player not found" : @"missing source or not loaded";
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                  messageAsString:message];
            }
        }
        
    
        // invalid result
        if (context==nil || pluginResult==nil) {
            NSString *message = [NSString stringWithFormat:@"invalid or missing audio context"];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                             messageAsString:message];
        }
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
   // }];
}



// -------------------------------------------------------------------------------------------------
// Setters
// -------------------------------------------------------------------------------------------------

- (void)setRoomProperties:(CDVInvokedUrlCommand*)command
{
    NSDictionary *dimensions = [command argumentAtIndex:0];
    NSDictionary *materials = [command argumentAtIndex:1];
    

    vraudio::RoomProperties room;
    // [0] width
    // [1] height
    // [2] depth
    room.dimensions[0] = [[dimensions valueForKey:@"width"] floatValue];
    room.dimensions[1] = [[dimensions valueForKey:@"height"] floatValue];
    room.dimensions[2] = [[dimensions valueForKey:@"depth"] floatValue];
    // [0] (-)ive x-axis wall (left)
    // [1] (+)ive x-axis wall (right)
    // [2] (-)ive y-axis wall (bottom)
    // [3] (+)ive y-axis wall (top)
    // [4] (-)ive z-axis wall (front)
    // [5] (+)ive z-axis wall (back)
    room.material_names[0] = [self getMaterialNameFrom:[materials valueForKey:@"left"]];
    room.material_names[1] = [self getMaterialNameFrom:[materials valueForKey:@"right"]];
    room.material_names[2] = [self getMaterialNameFrom:[materials valueForKey:@"down"]];
    room.material_names[3] = [self getMaterialNameFrom:[materials valueForKey:@"up"]];
    room.material_names[4] = [self getMaterialNameFrom:[materials valueForKey:@"front"]];
    room.material_names[5] = [self getMaterialNameFrom:[materials valueForKey:@"back"]];
    
    CDVPluginResult *pluginResult = nil;
    if (ResonanceAudioPlugin::setRoomProperties(room))
         pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                          messageAsString:@"uninitialized scene"];
        
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)setSpeedOfSound:(CDVInvokedUrlCommand*)command
{
    NSNumber *speed = [command argumentAtIndex:0];
    
    CDVPluginResult *pluginResult = nil;
    //if (ResonanceAudioPlugin::setSpeedOfSound([speed floatValue]))
         pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    //else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"uninitialized scene"];
        
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)setAmbisonicOrder:(CDVInvokedUrlCommand*)command
{
    NSNumber *order = [command argumentAtIndex:0];
    
    CDVPluginResult *pluginResult = nil;
    if (ResonanceAudioPlugin::setSourceRenderingMode([order intValue]))
         pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                          messageAsString:@"uninitialized scene"];
        
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)setListenerPosition:(CDVInvokedUrlCommand*)command
{
    NSNumber *x = [command argumentAtIndex:0];
    NSNumber *y = [command argumentAtIndex:1];
    NSNumber *z = [command argumentAtIndex:2];
    
    CDVPluginResult *pluginResult = nil;
    if (ResonanceAudioPlugin::setListenerPosition([x floatValue], [y floatValue], [z floatValue]))
         pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                          messageAsString:@"uninitialized listener"];
        
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)setListenerOrientation:(CDVInvokedUrlCommand*)command
{
    NSNumber *fx = [command argumentAtIndex:0];
    NSNumber *fy = [command argumentAtIndex:1];
    NSNumber *fz = [command argumentAtIndex:2];
    NSNumber *ux = [command argumentAtIndex:3];
    NSNumber *uy = [command argumentAtIndex:4];
    NSNumber *uz = [command argumentAtIndex:5];
    
    CDVPluginResult *pluginResult = nil;
    if (ResonanceAudioPlugin::setListenerOrientation([fx floatValue], [fy floatValue], [fz floatValue], [ux floatValue], [uy floatValue], [uz floatValue]))
         pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                          messageAsString:@"uninitialized listener"];
        
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}





- (MaterialName)getMaterialNameFrom:(NSString*)name
{
    if ([name isEqual:@"transparent"]) return kTransparent;
    if ([name isEqual:@"acoustic-ceiling-tiles"]) return kAcousticCeilingTiles;
    if ([name isEqual:@"brick-bare"]) return kBrickBare;
    if ([name isEqual:@"brick-painted"]) return kBrickPainted;
    if ([name isEqual:@"concrete-block-coarse"]) return kConcreteBlockCoarse;
    if ([name isEqual:@"concrete-block-painted"]) return kConcreteBlockPainted;
    if ([name isEqual:@"curtain-heavy"]) return kCurtainHeavy;
    if ([name isEqual:@"fiber-glass-insulation"]) return kFiberGlassInsulation;
    if ([name isEqual:@"glass-thin"]) return kGlassThin;
    if ([name isEqual:@"glass-thick"]) return kGlassThick;
    if ([name isEqual:@"grass"]) return kGrass;
    if ([name isEqual:@"linoleum-on-concrete"]) return kLinoleumOnConcrete;
    if ([name isEqual:@"marble"]) return kMarble;
    if ([name isEqual:@"metal"]) return kMetal;
    if ([name isEqual:@"parquet-on-concrete"]) return kParquetOnConcrete;
    if ([name isEqual:@"plaster-rough"]) return kPlasterRough;
    if ([name isEqual:@"plaster-smooth"]) return kPlasterSmooth;
    if ([name isEqual:@"plywood-panel"]) return kPlywoodPanel;
    if ([name isEqual:@"polished-concrete-or-tile"]) return kPolishedConcreteOrTile;
    if ([name isEqual:@"sheetrock"]) return kSheetrock;
    if ([name isEqual:@"water-or-ice-surface"]) return kWaterOrIceSurface;
    if ([name isEqual:@"wood-ceiling"]) return kWoodCeiling;
    if ([name isEqual:@"wood-panel'"]) return kWoodPanel;
    if ([name isEqual:@"uniform"]) return kUniform;
    
    return kTransparent;
}



// -------------------------------------------------------------------------------------------------
// Loggers
// -------------------------------------------------------------------------------------------------

void ResonanceAudioPlugin::NSLog_d(const char *message)
{
    #if DEBUG
    NSLog(@"[Resonance Audio plugin] %s", message);
    #endif
}

void ResonanceAudioPlugin::NSLog_w(const char *message)
{
    NSLog(@"[Resonance Audio plugin] warning: %s", message);
}

void ResonanceAudioPlugin::NSLog_e(const char *message)
{
    NSLog(@"[Resonance Audio plugin] error: %s", message);
}


@end
