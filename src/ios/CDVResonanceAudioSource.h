// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#import <Cordova/CDVPlugin.h>



@interface CDVResonanceAudioSource : CDVPlugin
{}

// Destroys the Resonance Audio source instance and frees resources
//- (void)destroy:(CDVInvokedUrlCommand*)command;

// Set the source's position (in meters), where origin is the center of the room
- (void)setPosition:(CDVInvokedUrlCommand*)command;
// Set the source's orientation using forward and up vectors
- (void)setOrientation:(CDVInvokedUrlCommand*)command;
// Set source's minimum distance (in meters)
- (void)setMinDistance:(CDVInvokedUrlCommand*)command;
// Set source's maximum distance (in meters)
- (void)setMaxDistance:(CDVInvokedUrlCommand*)command;
// Set source's rolloff model to use
- (void)setRolloff:(CDVInvokedUrlCommand*)command;
// Set source's directivity pattern
- (void)setDirectivityPattern:(CDVInvokedUrlCommand*)command;
// Set the source width (in degrees)
- (void)setSourceWidth:(CDVInvokedUrlCommand*)command;
// Set the gain of the near field effect
- (void)setSourceNearEffectGain:(CDVInvokedUrlCommand*)command;
// Set the source occlusion intensity
- (void)setSoundOcclusionIntensity:(CDVInvokedUrlCommand*)command;

@end
