// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


#import "CDVResonanceAudioSource.h"
#include "ResonanceAudioPlugin.h"





@interface CDVResonanceAudioSource()
@end

@implementation CDVResonanceAudioSource
{
}



- (void)pluginInitialize
{
    NSLog(@"Resonance Audio source initialized");
}

- (void)onAppTerminate
{
    // cleanup on app exit
}



// - (void)init:(CDVInvokedUrlCommand*)command
// {
//     CDVPluginResult *pluginResult = nil;
//     NSString *context = [command argumentAtIndex:0];
//     NSDictionary *options = [command argumentAtIndex:1];
    
//     if ( context!=nil ) {
//         #if CORDOVA_PLUGIN_FMOD
//         if ([context isEqual:@"fmod"]) {
//             ResonanceAudioPlugin::initialize([CDVFMOD initFMODSystem]);
//             pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
//         }
//         #endif
//     }
    
//     // invalid or missing audio context
//     if (pluginResult==nil) {
//         NSString *message = [NSString stringWithFormat:@"invalid or missing audio context '%@'", context];
//         pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
//                                          messageAsString:message];
//     }
    
//     [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
// }

- (int)getSourceId:(CDVInvokedUrlCommand*)command
{
    NSDictionary *arg = [command argumentAtIndex:0 withDefault:nil andClass:[NSDictionary class]];
    NSNumber *identifier = arg==nil ? [command argumentAtIndex:0] : [arg valueForKey:@"uuid"];

    if (identifier==nil) {
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                          messageAsString:@"Uninitialized source or invalid id"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return ResonanceAudioApi::kInvalidSourceId;
    }
    
    return [identifier intValue];
}



- (void)setPosition:(CDVInvokedUrlCommand*)command
{
    NSDictionary *arg = [command argumentAtIndex:0 withDefault:nil andClass:[NSDictionary class]];
    NSArray *position = [arg valueForKey:@"position"];
    

    NSNumber *x = position[0];
    NSNumber *y = position[1];
    NSNumber *z = position[2];
    
    int uuid = [self getSourceId:command];
    ResonanceAudioPlugin::setSourcePosition(uuid, [x floatValue], [y floatValue], [z floatValue]);
    
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:command.callbackId];
}

- (void)setOrientation:(CDVInvokedUrlCommand*)command
{
    NSDictionary *arg = [command argumentAtIndex:0 withDefault:nil andClass:[NSDictionary class]];
    NSArray *orientation = [arg valueForKey:@"orientation"];
    
    NSNumber *fx = orientation[0];
    NSNumber *fy = orientation[1];
    NSNumber *fz = orientation[2];
    NSNumber *ux = orientation[3];
    NSNumber *uy = orientation[4];
    NSNumber *uz = orientation[5];
    
    int uuid = [self getSourceId:command];
    ResonanceAudioPlugin::setSourceOrientation(uuid, [fx floatValue], [fy floatValue], [fz floatValue], [ux floatValue], [uy floatValue], [uz floatValue]);
    
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:command.callbackId];
}

- (void)setMinDistance:(CDVInvokedUrlCommand*)command
{
    NSDictionary *arg = [command argumentAtIndex:0 withDefault:nil andClass:[NSDictionary class]];
    NSNumber *distance = [arg valueForKey:@"distance"];
    
    int uuid = [self getSourceId:command];
    ResonanceAudioPlugin::setSourceMinDistance(uuid, [distance floatValue]);
    
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:command.callbackId];
}

- (void)setMaxDistance:(CDVInvokedUrlCommand*)command
{
    NSDictionary *arg = [command argumentAtIndex:0 withDefault:nil andClass:[NSDictionary class]];
    NSNumber *distance = [arg valueForKey:@"distance"];
    
    int uuid = [self getSourceId:command];
    ResonanceAudioPlugin::setSourceMaxDistance(uuid, [distance floatValue]);
    
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:command.callbackId];
}

- (void)setRolloff:(CDVInvokedUrlCommand*)command
{
    NSDictionary *arg = [command argumentAtIndex:0 withDefault:nil andClass:[NSDictionary class]];
    NSString *rolloff = [arg valueForKey:@"rolloff"];
    
    DistanceRolloffModel model = DistanceRolloffModel::kNone;
    if ([rolloff isEqualToString:@"logarithmic"]) model = DistanceRolloffModel::kLogarithmic;
    if ([rolloff isEqualToString:@"linear"]) model = DistanceRolloffModel::kLinear;
    
    int uuid = [self getSourceId:command];
    ResonanceAudioPlugin::setSourceRolloffModel(uuid, model);
    
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:command.callbackId];
}

- (void)setDirectivityPattern:(CDVInvokedUrlCommand*)command
{
    NSDictionary *arg = [command argumentAtIndex:0 withDefault:nil andClass:[NSDictionary class]];
    NSNumber *sharpness = [arg valueForKey:@"sharpness"];
    NSNumber *alpha = [arg valueForKey:@"alpha"];
    
    int uuid = [self getSourceId:command];
    ResonanceAudioPlugin::setSourceDirectivityPattern(uuid, [alpha floatValue], [sharpness floatValue]);
    
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:command.callbackId];
}

- (void)setSourceWidth:(CDVInvokedUrlCommand*)command
{
    NSDictionary *arg = [command argumentAtIndex:0 withDefault:nil andClass:[NSDictionary class]];
    NSNumber *width = [arg valueForKey:@"width"];
    
    int uuid = [self getSourceId:command];
    ResonanceAudioPlugin::setSourceSpread(uuid, [width floatValue]);
    
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:command.callbackId];
}

- (void)setSourceNearEffectGain:(CDVInvokedUrlCommand*)command
{
    NSDictionary *arg = [command argumentAtIndex:0 withDefault:nil andClass:[NSDictionary class]];
    NSNumber *gain = [arg valueForKey:@"gain"];
    
    int uuid = [self getSourceId:command];
    ResonanceAudioPlugin::setSourceNearEffectGain(uuid, [gain floatValue]);
    
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:command.callbackId];
}

- (void)setSoundOcclusionIntensity:(CDVInvokedUrlCommand*)command
{
    NSDictionary *arg = [command argumentAtIndex:0 withDefault:nil andClass:[NSDictionary class]];
    NSNumber *intensity = [arg valueForKey:@"intensity"];
    
    int uuid = [self getSourceId:command];
    ResonanceAudioPlugin::setSourceOcclusionIntensity(uuid, [intensity floatValue]);
    
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:command.callbackId];
}

@end
