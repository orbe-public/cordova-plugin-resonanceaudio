// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#import <Cordova/CDVPlugin.h>



@interface CDVResonanceAudio : CDVPlugin
{}

//+ (void)soundEnded:(NSString*)uuid;

// Initialize Resonance Audio environment / system
- (void)init:(CDVInvokedUrlCommand*)command;
// Closes Resonance Audio environment and frees resources
- (void)close:(CDVInvokedUrlCommand*)command;

// Turns on/off the reflections and reverberation
- (void)enableRoomEffects:(CDVInvokedUrlCommand*)command;

// Set the room's dimensions and wall materials
- (void)setRoomProperties:(CDVInvokedUrlCommand*)command;
// Set the speed of sound
- (void)setSpeedOfSound:(CDVInvokedUrlCommand*)command;
// Set the scene's desired ambisonic order
- (void)setAmbisonicOrder:(CDVInvokedUrlCommand*)command;
// Set the listener's position (in meters), where origin is the center of the room
- (void)setListenerPosition:(CDVInvokedUrlCommand*)command;
// Set the listener's orientation using forward and up vectors
- (void)setListenerOrientation:(CDVInvokedUrlCommand*)command;

// Create a new source for the scene
- (void)createSource:(CDVInvokedUrlCommand*)command;

@end
