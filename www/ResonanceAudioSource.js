// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.



/**
 * Manage the Resonance Audio sources on the scene.
 * Implement the web source reference, based on the Resonance Audio SDK on native side.
 * https://resonance-audio.github.io/resonance-audio/reference/web/Source
 *
 * @param {Number} uuid - The source unique id.
 * @class
 */
var ResonanceAudioSource = function (uuid) {
    this.uuid = uuid;
}





/**
 * Set the source's position (in meters), where origin is the center of the room.
 *
 * @param {Number} x
 * @param {Number} y
 * @param {Number} z
 * @param {ResonanceAudioSource~error} onError - The callback function to be called on error.
 */
ResonanceAudioSource.prototype.setPosition = function (x, y, z, onError) {
    var position = [x, y, z];
    cordova.exec(null, onError, "ResonanceAudioSource", "setPosition", [{ uuid: this.uuid, position }]);
},

/**
 * Set the source's orientation using forward and up vectors.
 *
 * @param {Number} forwardX
 * @param {Number} forwardY
 * @param {Number} forwardZ
 * @param {Number} upX
 * @param {Number} upY
 * @param {Number} upZ
 * @param {ResonanceAudioSource~error} onError - The callback function to be called on error.
 */
ResonanceAudioSource.prototype.setOrientation = function (forwardX, forwardY, forwardZ, upX, upY, upZ, onError) {
    var orientation = [forwardX, forwardY, forwardZ, upX, upY, upZ];
    cordova.exec(null, onError, "ResonanceAudioSource", "setOrientation", [{ uuid: this.uuid, orientation }]);
},

/**
 * Set source's minimum distance (in meters).
 *
 * @param {Number} distance
 * @param {ResonanceAudioSource~error} onError - The callback function to be called on error.
 */
ResonanceAudioSource.prototype.setMinDistance = function (distance, onError) {
    cordova.exec(null, onError, "ResonanceAudioSource", "setMinDistance", [{ uuid: this.uuid, distance }]);
},

/**
 * Set source's maximum distance (in meters).
 *
 * @param {Number} distance
 * @param {ResonanceAudioSource~error} onError - The callback function to be called on error.
 */
ResonanceAudioSource.prototype.setMaxDistance = function (distance, onError) {
    cordova.exec(null, onError, "ResonanceAudioSource", "setMaxDistance", [{ uuid: this.uuid, distance }]);
},

/**
 * Set source's rolloff model to use.
 *
 * @param {String} rolloff -  The rolloff model name, either 'logarithmic', 'linear' or 'none'
 * @param {ResonanceAudioSource~error} onError - The callback function to be called on error.
 */
ResonanceAudioSource.prototype.setRolloff = function (rolloff, onError) {
    cordova.exec(null, onError, "ResonanceAudioSource", "setRolloff", [{ uuid: this.uuid, rolloff }]);
},

/**
 * Set source's directivity pattern (defined by alpha), where 0 is an omnidirectional pattern, 1 is 
 * a bidirectional pattern, 0.5 is a cardiod pattern. The sharpness of the pattern is increased 
 * exponentially.
 *
 * @param {Number} alpha - Determines the directivity pattern in range [0, 1].
 * @param {Number} sharpness - Determines the sharpness of the directivity pattern. Higher values will result in narrower and sharper directivity patterns. Range [1, inf).
 * @param {ResonanceAudioSource~error} onError - The callback function to be called on error.
 */
ResonanceAudioSource.prototype.setDirectivityPattern = function (alpha, sharpness, onError) {
    cordova.exec(null, onError, "ResonanceAudioSource", "setDirectivityPattern", [{ uuid: this.uuid, alpha, sharpness }]);
},

/**
 * Set the source width (in degrees). Where 0 degrees is a point source and 360 degrees is an 
 * omnidirectional source.
 *
 * @param {Number} width
 * @param {ResonanceAudioSource~error} onError - The callback function to be called on error.
 */
ResonanceAudioSource.prototype.setSourceWidth = function (width, onError) {
    cordova.exec(null, onError, "ResonanceAudioSource", "setSourceWidth", [{ uuid: this.uuid, width }]);
},

/**
 * Set the gain (linear) of the near field effect.
 *
 * @param {Number} gain - The gain of the near field effect in range [0, 9] corresponding to approx. (-Inf, +20dB].
 * @param {ResonanceAudioSource~error} onError - The callback function to be called on error.
 */
ResonanceAudioSource.prototype.setSourceNearEffectGain = function (gain, onError) {
    cordova.exec(null, onError, "ResonanceAudioSource", "setSourceNearEffectGain", [{ uuid: this.uuid, gain }]);
},

/**
 * Set the source occlusion intensity.
 *
 * @param {Number} intensity - The number of occlusions occurred for the sound object.
 * The value can be set to fractional for partial occlusions in range [0, inf).
 * @param {ResonanceAudioSource~error} onError - The callback function to be called on error.
 */
ResonanceAudioSource.prototype.setSoundOcclusionIntensity = function (intensity, onError) {
    cordova.exec(null, onError, "ResonanceAudioSource", "setSoundOcclusionIntensity", [{ uuid: this.uuid, intensity }]);
},

module.exports = ResonanceAudioSource;