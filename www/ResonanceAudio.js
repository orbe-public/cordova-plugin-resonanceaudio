// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.



/**
 * Manage sources, room and listener models.
 * Implement the web reference, based on the Resonance Audio SDK on native side.
 * https://resonance-audio.github.io/resonance-audio/reference/web/ResonanceAudio
 *
 * @param {String} context - The associated audio context.
 * @param {ResonanceAudio~ResonanceAudioOptions} options - Options for constructing a new ResonanceAudio scene.
 * @class
 */
var ResonanceAudio = function (context, options) {
    // @todo: delete these lines when another audio context is defined
    // force the only available context for the moment
    if (arguments.length === 1) options = context;
    this.context = ResonanceAudio.FMOD_CONTEXT;
    //this.context = context;

    var onError = function (e) {
        console.error('Unable to initialize ResonanceAudio system:', e);
    };

    if (options == undefined) options = {};
    if (options.listenerPosition == undefined) options.listenerPosition = [0, 0, 0];
    if (options.dimensions == undefined) options.dimensions = { width: 0, height: 0, depth: 0};
    if (options.materials == undefined) options.materials = {};
    if (options.speedOfSound == undefined) options.speedOfSound = 343;

    // initialize Resonance Audio environment / system
    cordova.exec(null, onError, "ResonanceAudio", "init", [this.context, options]);
}

// available audio context
ResonanceAudio.FMOD_CONTEXT = "fmod";





/**
 * Closes Resonance Audio environment and frees resources.
 *
 * @param {ResonanceAudio~success} onSuccess - The callback function to be called on success.
 * @param {ResonanceAudio~error} onError - The callback function to be called on error.
 */
ResonanceAudio.prototype.close = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "ResonanceAudio", "close", []);
    this.context = null;
},

/**
 * Turns on/off the reflections and reverberation.
 * Enabling this parameter increases processor usage and may produce audio latency, depending on the
 * Ambisonic order used.
 *
 * @param {Boolean} enable
 * @param {ResonanceAudio~error} onError - The callback function to be called on error.
 */
ResonanceAudio.prototype.enableRoomEffects = function (enable, onError) {
    cordova.exec(null, onError, "ResonanceAudio", "enableRoomEffects", [enable]);
},

/**
 * Set the room's dimensions and wall materials.
 *
 * @param {Object} dimensions - Room dimensions (in meters).
 * @param {Object} materials - Named acoustic materials per wall.
 * @param {ResonanceAudio~error} onError - The callback function to be called on error.
 */
ResonanceAudio.prototype.setRoomProperties = function (dimensions, materials, onError) {
    cordova.exec(null, onError, "ResonanceAudio", "setRoomProperties", [dimensions, materials]);
},

/**
 * Set the speed of sound.
 *
 * @param {Number} speed
 * @param {ResonanceAudio~error} onError - The callback function to be called on error.
 */
ResonanceAudio.prototype.setSpeedOfSound = function (speed, onError) {
    cordova.exec(null, onError, "ResonanceAudio", "setSpeedOfSound", [speed]);
},

/**
 * Set the scene's desired ambisonic order.
 * As Ambisonic order increases, sound wave simulation becomes more accurate, but it costs more
 * resources: 
 *  - First Order Ambisonics: over a virtual array of 8 loudspeakers arranged in a cube configuration
 *    around the listener's head.
 *  - Second Order Ambisonics: over a virtual array of 12 loudspeakers arranged in a dodecahedral
 *    configuration (using faces of the dodecahedron).
 *  - Third Order Ambisonics: over a virtual array of 26 loudspeakers arranged in a Lebedev grid.
 *
 * @param {Number} order - The desired ambisonic order in range [1, 3].
 * @param {ResonanceAudio~error} onError - The callback function to be called on error.
 */
ResonanceAudio.prototype.setAmbisonicOrder = function (order, onError) {
    cordova.exec(null, onError, "ResonanceAudio", "setAmbisonicOrder", [order]);
},

/**
 * Set the listener's orientation using forward and up vectors.
 *
 * @param {Number} forwardX
 * @param {Number} forwardY
 * @param {Number} forwardZ
 * @param {Number} upX
 * @param {Number} upY
 * @param {Number} upZ
 * @param {ResonanceAudio~error} onError - The callback function to be called on error.
 */
ResonanceAudio.prototype.setListenerOrientation = function (forwardX, forwardY, forwardZ, upX, upY, upZ, onError) {
    cordova.exec(null, onError, "ResonanceAudio", "setListenerOrientation", [forwardX, forwardY, forwardZ, upX, upY, upZ]);
},

/**
 * Set the listener's position (in meters), where origin is the center of the room.
 *
 * @param {Number} x
 * @param {Number} y
 * @param {Number} z
 * @param {ResonanceAudio~error} onError - The callback function to be called on error.
 */
ResonanceAudio.prototype.setListenerPosition = function (x, y, z, onError) {
    cordova.exec(null, onError, "ResonanceAudio", "setListenerPosition", [x, y, z]);
},

/**
 * Create a new source model to spatialize for the scene.
 *
 * @param {*} source - The associated source instance.
 * @param {ResonanceAudio~SourceOptions} options - The options for constructing a new Source.
 * @param {ResonanceAudio~success} onSuccess - The callback function to be called on success.
 * @param {ResonanceAudio~error} onError - The callback function to be called on error.
 */
ResonanceAudio.prototype.createSource = function (source, options, onSuccess, onError) {
    if (!this.context) onError('audio context not found');
    else {
        // use defaults for undefined arguments
        if (options == undefined)  options = {};
        if (options.position == undefined) options.position = [0, 0, 0];
        if (options.forward == undefined) options.forward = [0, 0, -1];
        if (options.up == undefined) options.up = [0, 1, 0];
        if (options.minDistance == undefined) options.minDistance = 1;
        if (options.maxDistance == undefined) options.maxDistance = 1000;
        if (options.rolloff == undefined) options.rolloff = 'logarithmic';
        if (options.gain == undefined) options.gain = 1;
        if (options.alpha == undefined) options.alpha = 0;
        if (options.sharpness == undefined) options.sharpness = 1;
        if (options.sourceWidth == undefined) options.sourceWidth = 0;

        var callback = function (id) { onSuccess(new ResonanceAudioSource(id)); }
        cordova.exec(callback, onError, "ResonanceAudio", "createSource", [source, options]);
    }
};

module.exports = ResonanceAudio;