const fs = require('fs');
const path = require('path');

module.exports = (context) => {
    console.log(context.opts.cordova.plugins);
    // ios platform
    if (context.opts.cordova.platforms.includes('ios')) {
        const cordovaBuildConfDir = path.join(context.opts.projectRoot, 'platforms/ios/cordova');
        const buildConfigurations = ['debug', 'release'];

        // delete config file and
        // remove include in the cordova build configuration file
        buildConfigurations.forEach((configuration) => {
            const cfg = path.join(context.opts.projectRoot, `platforms/ios/cordova-plugin-resonanceaudio-${configuration}.xcconfig`);
            fs.rm(cfg, (e)=>{});
            try {
                const xconfig = path.join(cordovaBuildConfDir, `build-${configuration}.xcconfig`);
                let content = fs.readFileSync(xconfig, 'utf8');
                content = content.replace(/.*resonanceaudio.*/g, '');
                content = content.trimEnd();
                fs.writeFileSync(xconfig, content, 'utf8');
            }
            catch (error) {
                console.log(error);
            }
        });
    }

    // android platform
    // remove the orbe-audio library CMake config (if needed)
    if (context.opts.cordova.platforms.includes('android')) {
        const plugins = context.opts.cordova.plugins;
        if (plugins.includes('cordova-plugin-fmod')) return; // do not delete file

        const file = path.join(context.opts.projectRoot, 'platforms/android/app/src/orbe-audio-lib/CMakeLists.txt');
        fs.stat(file, (err, stats) => {
            if (stats) fs.rm(file, (err) => {
                if (err) throw err;
            });
        });
    }
};